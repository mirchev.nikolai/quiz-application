import pool from './pool.js';

const findUserTake = async (id: number, userId: number) => {
  const sql: string = `
  SELECT *
  FROM takes 
  WHERE users_id = ? AND quiz_id = ?
  `;

  const result = await pool.query(sql, [userId, id]);
  return result[0];
};

const getActiveQuiz = async (userId: number) => {
  const sql: string = `
  SELECT *
  FROM takes
  WHERE users_id = ? AND is_submitted = 0
  `;

  const result = await pool.query(sql, [userId]);
  return result[0];
};

const newTake = async (id: number, userId: number, startingTime: Date) => {
  const sql: string = `
  INSERT INTO takes(quiz_id, users_id, started_at, is_submitted)
  VALUES (?, ?, ?,'0');
  `;

  const result = await pool.query(sql, [id, userId, startingTime]);
  return {
    id: result.insertId,
    quiz_id: id,
    users_id: userId,
    started_at: startingTime,
    is_submitted: '0',
  };
};

const submitAnswers = async (id: number, answers: string[]) => {
  let sql: string = answers.reduce((acc, el) => {
    return (acc += `INSERT INTO take_answers(take_id, answers_id)VALUES (${id}, ${el});
    `);
  }, ``);

  return await pool.query(sql);
};

const submitQuiz = async (id: number, userId: number) => {
  const sql: string = `
  UPDATE takes
  SET is_submitted = 1
  WHERE quiz_id = ? AND users_id = ? AND is_submitted = 0;
  `;

  return await pool.query(sql, [id, userId]);
};

const getStudentScore = async (id: number) => {
  const sql: string = `
  SELECT avg(takes.result)as avg_result, count(takes.id) as total_takes, users.username, users.firstname,users.lastname 
  FROM takes, users 
  WHERE users.roles_id=2 
  AND takes.users_id = ?  
  AND users.id = takes.users_id
  AND takes.is_submitted=1;
  `;

  const result = await pool.query(sql, [id]);

  return result;
};

const getScoreOnTake = async (id: number, userId: number) => {
  const sql: string = `
  SELECT takes.result, users.username, users.firstname,users.lastname 
  FROM takes, users 
  WHERE users.roles_id = 2
  AND takes.quiz_id = ? 
  AND takes.users_id = ?
  AND users.id = takes.users_id
  AND takes.is_submitted=1;
  `;

  const result = await pool.query(sql, [id, userId]);
  return result[0];
};

export default {
  findUserTake,
  newTake,
  getActiveQuiz,
  submitAnswers,
  submitQuiz,
  getStudentScore,
  getScoreOnTake,
};
