import { Button } from '@material-ui/core';
import React, { useState } from 'react';
import SingleChoiceAnswer from './SingleChoiceAnswer';

const SingleAnswerQuestion: React.FC = () => {
    const [answers, setAnswers] = useState([
        <div key={1}><SingleChoiceAnswer /></div>,
        <div key={2}><SingleChoiceAnswer /></div>,
    ]);

    const removeLastAnswer = () => {
        if (answers.length > 2) {
            setAnswers(answers.slice(0, -1))
        }
    }

    return(
        <div className="answers">
        {answers && answers.map((answer, index) => <div key={index + 2}>{answer}</div>)}
        <Button size="small" variant="contained" color="primary" onClick={() => setAnswers([...answers, <SingleChoiceAnswer />])}>Add Answer</Button>
        &nbsp;
        <Button size="small" variant="contained" color="primary" onClick={removeLastAnswer}>Remove Last</Button>
        </div>
    );
}

export default SingleAnswerQuestion;