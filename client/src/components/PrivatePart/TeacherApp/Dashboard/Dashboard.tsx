import React from 'react';
import CreateQuizForm from '../CreateQuiz/CreateQuizForm';
import MultiAnswerQuestion from '../CreateQuiz/MultiAnswerQuestion';
import SingleAnswerQuestion from '../CreateQuiz/SingleAnswerQuestion';

const Dashboard: React.FC = () => {
    return (
        <div>
            <CreateQuizForm />
            <MultiAnswerQuestion />
            <SingleAnswerQuestion />
        </div>
    );
}

export default Dashboard;