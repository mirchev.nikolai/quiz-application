import { log } from 'console';
import serviceErrors from './service-errors.js';

const getAllCategoriesPaged = (data: {
  getCategoryBy: Function;
  getSearchCategoryCount: Function;
  getAllCategories: Function;
  allCategoriesCount: Function;
}) => {
  return async (search: undefined | string, column: string, page: number) => {
    const limit = 20;
    const offset = (page - 1) * limit;

    if (search) {
      const category = await data.getCategoryBy(search, column, limit, offset);

      const [{ count }] = await data.getSearchCategoryCount(search, column);

      return {
        category: category,
        count: count,
        currentPage: page,
        hasNext: offset + limit < count,
        hasPrevious: page > 1,
      };
    } else {
      const category = await data.getAllCategories(limit, offset);
      const [{ count }] = await data.allCategoriesCount(search, column);

      return {
        category: category,
        count: count,
        currentPage: page,
        hasNext: offset + limit < count,
        hasPrevious: page > 1,
      };
    }
  };
};

const getCategoryById = (data: { categoryBy: Function }) => {
  return async (id: string) => {
    const category = await data.categoryBy('id', id);
    if (!category) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        category: null,
      };
    }

    return { error: null, category: category };
  };
};

const getAllCategories = (data: { allCategories: Function }) => {
  return async () => {
    const categories = await data.allCategories();
    return categories;
  };
};

const createCategory = (data: {
  categoryBy: Function;
  createCategory: Function;
}) => {
  return async (
    categoryData: {
      name: string;
      description: string;
      images_id: string;
    },
    userId: number
  ) => {
    const { name, description, images_id } = categoryData;

    const existingCategory = await data.categoryBy('name', name);

    if (existingCategory) {
      return {
        error: serviceErrors.DUPLICATE_RECORD,
        category: null,
      };
    }

    const category = await data.createCategory(
      name,
      description,
      +images_id,
      +userId
    );
    return { error: null, category: category };
  };
};

export default {
  getAllCategoriesPaged,
  getAllCategories,
  getCategoryById,
  createCategory,
};
