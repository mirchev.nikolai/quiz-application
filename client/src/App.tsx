import React, { useState } from 'react';
import decode from 'jwt-decode';
import AuthContext from './auth/AuthContext';
import './App.css';
import TeacherApp from './components/PrivatePart/TeacherApp/TeacherApp';
import PublicApp from './components/PublicPart/PublicApp';

interface IDecodedToken {
  sub: number;
  username: string;
  role: string;
  iat: number;
  exp: number;
}

const App: React.FC = () => {
  const token = localStorage.getItem('token');
  const [user, setUser] = useState<IDecodedToken | null>(token ? decode(token) : null);

  return (
    <div className="App">
      <AuthContext.Provider value={{ user: user, setUser: setUser }}>
      {user ? user.role === 'teacher' ? <TeacherApp /> : <>Going to STUDENT APP</> : <PublicApp />}
      </AuthContext.Provider>
    </div>
  );
}

export default App;
