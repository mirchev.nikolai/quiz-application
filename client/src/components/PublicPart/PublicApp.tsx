import React from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import Login from './Login/Login';
import Register from './Register/Register';

const PublicApp: React.FC = () => {
  return (
    <BrowserRouter>
      <div className='main-content'>
        <Switch>
          <Redirect path='/' exact to='/login' />
          <Route path='/login' component={Login} />
          <Route path='/register' component={Register} />
          <Route path='*' component={Login} />
        </Switch>
      </div>
    </BrowserRouter>
  );
};

export default PublicApp;