import express, { Request, Response } from 'express';
import createToken from '../auth/create-token';
import serviceErrors from '../services/service-errors';
import usersData from '../data/users-data';
import usersService from '../services/users-service';
import {
  bodyValidator,
  createUserSchema,
} from '../validations/validation-exports.js';

const usersController = express.Router();

usersController
  //GET ALL USERS OPTIONAL SEARCH
  .get('/', async (req: Request, res: Response) => {
    let search;
    let column;
    let page;

    if (req.query && req.query.search) {
      search = (req.query as any).search;
    }
    if (req.query && req.query.column) {
      column = (req.query as any).column;
    }
    if (req.query && req.query.page) {
      page = (req.query as any).page;
    }

    const result = await usersService.getAllUsers(usersData)(
      search,
      column ? column : 'username',
      page ? +page : 1
    );
    res.status(200).send(result);
  })

  // CREATE USER (REGISTER STUDENT)
  .post('/', bodyValidator(createUserSchema), async (req:Request, res:Response) => {
    const createData: {
      username: string;
      firstname: string;
      lastname: string;
      password: string;
    } = req.body;

    const { error, user } = await usersService.registerUser(usersData)(
      createData
    );
    if (error === serviceErrors.DUPLICATE_RECORD) {
      res.status(409).send({ message: 'Username not available' });
    } else {
      res.status(201).send(user);
    }
  })

  // SIGN IN
  .post('/signin', async (req:Request, res:Response) => {
    const { username, password } = req.body;
    const { error, user } = await usersService.signInUser(usersData)(
      username,
      password
    );

    if (error === serviceErrors.INVALID_SIGNIN) {
      res.status(400).send({
        message: 'Invalid username/password',
      });
    } else {
      const payload = {
        sub: user.id,
        username: user.username,
        role: user.role,
      };
      const token = createToken(payload);

      res.status(200).send({
        token: token,
      });
    }
  });

export default usersController;
