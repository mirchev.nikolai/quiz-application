import React from 'react';
import { AccountCircle as AccountCircleIcon } from '@material-ui/icons';
import { useForm } from 'react-hook-form';
import {
    usernameReqs,
    passwordReqs,
  } from '../../../utils/validation/constants';
import {
  Avatar,
  Container,
  CssBaseline,
  Button,
  Typography,
} from '@material-ui/core';
import { CssTextField, useStyles } from './styles';
import { useHistory } from 'react-router-dom';
import httpProvider from '../../../providers/http-provider';
import decode from 'jwt-decode';
import { useAuth } from '../../../auth/AuthContext';
import { BASE_URL } from '../../../common/constants';


interface LoginInput {
  username: string;
  password: string;
}

const SignInForm = () => {
  const classes = useStyles();
  const { register, handleSubmit, errors } = useForm<LoginInput>();
  const { setUser } = useAuth();
  const history = useHistory();

  const onSubmit = (data: LoginInput) => {
    httpProvider.post(`${BASE_URL}/users/signin`, data)
        .then((data) => {
          if (data.message) {
            alert(data.message);
          } else {
            localStorage.setItem('token', data.token);
            const user = decode(data.token);
            setUser(user);
          }
        });
  };

  return (
    <Container component='main' maxWidth='xs'>
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <AccountCircleIcon style={{ fontSize: 45 }} />
        </Avatar>
        <Typography component='h1' variant='h4'>
          Sign in
        </Typography>
        <form className={classes.form} noValidate onSubmit={handleSubmit(onSubmit)}>
          <CssTextField
            name='username'
            label='Username'
            variant='outlined'
            margin='normal'
            inputRef={register(usernameReqs)}
            autoComplete='username'
            placeholder='Your username...'
            fullWidth
            required
            autoFocus
          />
          {errors.username && (
            <span className={classes.error}>{errors.username.message}</span>
          )}

          <CssTextField
            name='password'
            label='Password'
            type='password'
            variant='outlined'
            margin='normal'
            inputRef={register(passwordReqs)}
            placeholder='Your password...'
            fullWidth
            required
            autoComplete='current-password'
          />
          {errors.password && (
            <span className={classes.error}>{errors.password.message}</span>
          )}

          <Button
            type='submit'
            fullWidth
            variant='contained'
            className={classes.submit}
          >
            Sign In
          </Button>
          <Button
            fullWidth
            variant='contained'
            className={classes.submit}
            onClick={() => history.push('/register')}
            >Create an Account
          </Button>
        </form>
      </div>
    </Container>
  );
};

export default SignInForm;


// import React from 'react';
// import decode from 'jwt-decode';
// import { useForm } from 'react-hook-form';
// import { useAuth } from '../../../auth/AuthContext';
// import { BASE_URL } from '../../../common/constants';
// import httpProvider from '../../../providers/http-provider';
// import {
//     usernameReqs,
//     passwordReqs,
//   } from '../../../utils/validation/constants';
// import './Login.css';

// interface ILoginData {
//     username: string;
//     password: string;
// }

// const Login: React.FC = () => {
    // const { setUser } = useAuth();
//     const { register, handleSubmit, errors } = useForm<ILoginData>();

//     const login = (data: ILoginData) => {
//         console.log(data);
        // httpProvider.post(`${BASE_URL}/users/signin`, data)
        // .then((data) => {
        //   if (data.message) {
        //     alert(data.message);
        //   } else {
        //     localStorage.setItem('token', data.token);
        //     const user = decode(data.token);
        //     setUser(user);
        //   }
        // });
//       };

//     return (
//         <>
//       <p className='sign_input'>Log in to continue to the website</p>
//       <form className='form-container' onSubmit={handleSubmit(login)}>
//         <input
//           className='input-form'
//           name='username'
//           type='text'
//           placeholder='Username...'
//           ref={register(usernameReqs)}
//         />
//         {errors.username && (
//           <span className='error-span'>
//             <p>Please, enter valid username</p>
//           </span>
//         )}
//         {errors.username && (
//           <span className='error-span'>
//             <p>{errors.username.message}</p>
//           </span>
//         )}
//         <br />
//         <input
//           className='input-form'
//           name='password'
//           type='password'
//           placeholder='Password...'
//           ref={register(passwordReqs)}
//         />
//         {errors.password && (
//           <span className='error-span'>
//             <p>Please, enter valid password</p>
//           </span>
//         )}
//         {errors.password && (
//           <span className='error-span'>
//             <p>{errors.password.message}</p>
//           </span>
//         )}
//         <br />
//         <input className='input-submit' type='submit' />
//       </form>
//     </>
//     );
// }

// export default Login;