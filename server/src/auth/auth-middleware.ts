import passport from 'passport';
import { Request, Response, NextFunction } from 'express';

const authMiddleware = passport.authenticate('jwt', { session: false });

const roleMiddleware = (roleName: Array<string>) => {
  return (req: Request, res: Response, next: NextFunction) => {
    const user: {
      role?: string;
    } = req.user!;
    const role = roleName.find((r: string) => r === user.role);

    if (req.user && user.role === role) {
      next();
    } else {
      res.status(403).send({
        message: 'Resource is forbidden.',
      });
    }
  };
};

export { authMiddleware, roleMiddleware };
