import express, { Application, Request, Response, NextFunction } from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import passport from 'passport';
import { PORT } from './config';
import jwtStrategy from './auth/strategy.js';

import usersController from './controllers/users-controller';
import takesController from './controllers/takes-controller';
import categoriesController from './controllers/categories-controller';
import quizController from './controllers/quiz-controller';
import scoresController from './controllers/scores-controller';

import { authMiddleware, roleMiddleware } from './auth/auth-middleware.js';
import { registeredUsers, teachersOnly } from './config';

const app: Application = express();

passport.use(jwtStrategy);

// CORS Policy and Body Parser Third-party Middleware
app.use(cors(), bodyParser.json());
app.use(passport.initialize());
app.use(helmet());

app.use('/users', usersController);
app.use(
  '/takes',
  authMiddleware,
  roleMiddleware(registeredUsers),
  takesController
);
app.use(
  '/categories',
  authMiddleware,
  roleMiddleware(registeredUsers),
  categoriesController
);
app.use(
  '/scores',
  authMiddleware,
  roleMiddleware(registeredUsers),
  scoresController
);
app.use('/quiz', quizController);

app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
  res.status(500).send({
    message:
      'An unexpected error occurred, stay tuned - we will fix it in no time!',
  });
});

app.all('*', (req: Request, res: Response) =>
  res.status(404).send({ message: 'Resource not found!' })
);

app.listen(PORT, () => console.log(`App is listening on port ${PORT}`));
