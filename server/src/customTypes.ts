export type Answer = {
  id?: number;
  text: string;
  is_correct: number;
};
export type Question = {
  id?: number;
  name: string;
  points: number;
  answers: Answer[];
};
export type Quiz = {
  id?: number;
  category: number;
  userId: number;
  title: string;
  questions: Question[];
};

export type RawQuizData = {
    quizID: number;
    quizTitle: string;
    quizCategory: number;
    quizCreatorID: number;
    questionID: number;
    questionText: string;
    questionPoints: number;
    answerID: number;
    answerText: string;
    is_correct: number;
}
