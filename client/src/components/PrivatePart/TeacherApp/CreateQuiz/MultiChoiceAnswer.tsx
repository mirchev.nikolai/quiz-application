import { Checkbox, TextField } from '@material-ui/core';
import React, { useState } from 'react';

const MultiChoiceAnswer: React.FC = () => {
    const [checked, setChecked] = useState(false);
    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setChecked(event.target.checked);
      };

    return (
        <div>
            <Checkbox
                checked={checked}
                onChange={handleChange}
            />
            <TextField id="standard-basic" label="answer..." size="small"/>
        </div>
    );
}

export default MultiChoiceAnswer;