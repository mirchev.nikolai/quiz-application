import { Request, Response, NextFunction } from 'express';

export const bodyValidator = (schema: any) => {
  return (req: Request, res: Response, next: NextFunction) => {
    const body = req.body;
    const validations = Object.keys(schema);

    const fails = validations
      .map((validator: string) => schema[validator](body[validator]))
      .filter((e) => e !== null);

    if (fails.length > 0) {
      res.status(400).send(fails);
    } else {
      next();
    }
  };
};
