import { Button, FormControl, InputLabel, MenuItem, Select, TextField } from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import React, { useState } from 'react';
import { useAuth } from '../../../../auth/AuthContext';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        formControl: {
            margin: theme.spacing(1),
            minWidth: 280,
        },
        selectEmpty: {
            marginTop: theme.spacing(1),
        },
        root: {
            margin: theme.spacing(1),
            minWidth: 280,
        },
    }),
);
const CreateQuizForm: React.FC = () => {
    const { user } = useAuth();
    const classes = useStyles();
    const [category, setCategory] = useState('');
    const [quiz, updateQuiz] = useState({
        category: null,
        userId: user?.sub,
        title: '',
        timelimit: 0,
        questions: []
    });


    const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
        setCategory(event.target.value as string);
        console.log(event.target.value)
      };


    return (
        <div>
            <FormControl className={classes.root}>
                <InputLabel id="select-category">Select Category</InputLabel>
                <Select
                    className={classes.root}
                    labelId="select-category"
                    id="demo-simple-select"
                    value={category}
                    required
                    onChange={handleChange}
                >
                <MenuItem value={11}>Math</MenuItem>
                <MenuItem value={21}>History</MenuItem>
                <MenuItem value={15}>Music</MenuItem>
                </Select>
                <TextField
                    className={classes.root}
                    id="standard-number"
                    label="Timelimit in minutes"
                    type="number"
                    required
                    variant="outlined"
                    InputLabelProps={{
                        shrink: true,
                    }}
                    inputProps={{min: "1"}}
                />
                <TextField className={classes.root} required id="title" label="Quiz Title..." defaultValue="" variant="outlined" />
            <div>
            <Button variant="contained" color="primary">Add Multi</Button>&nbsp;
            <Button variant="contained" color="primary">Add Single</Button>
            </div>
            </FormControl>
        </div>
    );
}

export default CreateQuizForm;