import serviceErrors from './service-errors.js';

const takeQuiz = (data: {
  findUserTake: Function;
  newTake: Function;
  getActiveQuiz: Function;
}) => {
  return async (
    id: number,
    user: {
      role?: string;
      id?: string;
    }
  ) => {
    const alreadyTaken = await data.findUserTake(+id, +user.id!);

    if (alreadyTaken && user.role !== 'teacher') {
      return {
        error: serviceErrors.ALREADY_TAKEN,
        take: null,
      };
    }

    const activeQuiz = await data.getActiveQuiz(+user.id!);
    if (activeQuiz) {
      return {
        error: serviceErrors.ANOTHER_ACTIVE_QUIZ,
        take: null,
      };
    }

    const startingTime = new Date();
    const take = await data.newTake(id, +user.id!, startingTime);

    return { error: null, take: take };
  };
};

const submitQuiz = (data: {
  getActiveQuiz: Function;
  submitAnswers: Function;
  submitQuiz: Function;
}) => {
  return async (
    id: number,
    user: {
      role?: string;
      id?: string;
    },
    answers: string[]
  ) => {
    const activeQuiz = await data.getActiveQuiz(+user.id!);

    if (!activeQuiz || +activeQuiz.quiz_id !== +id) {
      return {
        error: serviceErrors.INVALID_OPERATION,
        submission: null,
      };
    }

    const takeId = activeQuiz.id;

    const _ = await data.submitAnswers(takeId, answers);

    const submission = await data.submitQuiz(id, +user.id!);

    return { error: null, submission: submission };
  };
};

const studentScore = (data: { getStudentScore: Function }) => {
  return async (user: { role?: string; id?: string }) => {
    const userId = user.id;

    const result = await data.getStudentScore(userId);

    if (!result[0].avg_result) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        result: null,
      };
    }
    return { error: null, result: result };
  };
};

const scoreOnTake = (data: {
  getScoreOnTake: Function;
  findUserTake: Function;
}) => {
  return async (
    id: number,
    user: {
      role?: string;
      id?: string;
    }
  ) => {
    const userId = user.id!;

    const record = await data.findUserTake(+id, +userId);
    if (!record) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        result: null,
      };
    }
    const result = await data.getScoreOnTake(+id, +userId);

    return {
      error: null,
      result: result,
    };
  };
};

export default {
  takeQuiz,
  submitQuiz,
  studentScore,
  scoreOnTake,
};
