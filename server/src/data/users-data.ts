import pool from './pool.js';

const createUser = async (
  username: string,
  password: string,
  firstname: string,
  lastname: string
) => {
  const sql = `
  INSERT INTO users(username, password, firstname,lastname,roles_id)
  VALUES (?,?,?,?,2)
  `;

  const result = await pool.query(sql, [
    username,
    password,
    firstname,
    lastname,
  ]);

  return {
    id: result.insertId,
    username: username,
  };
};

const getUserBy = async (column: string, searchBy: string) => {
  const sql = `
  SELECT u.id, u.username, u.password, u.firstname, u.lastname, r.name as role
  FROM users u
  JOIN roles r ON u.roles_id = r.id
  WHERE u.${column} = ?;
  `;

  const result = await pool.query(sql,[searchBy]);
  return result[0];
};

const getAllUsers = async (limit: number, offset: number) => {
  const sql = `
  SELECT id,username,firstname,lastname,roles_id
  FROM users
  LIMIT ? OFFSET ?;
  `;
  return await pool.query(sql, [limit, offset]);
};

const allUsersCount = async () => {
  const sql = `
  SELECT COUNT(*) as count FROM users 
  `;

  return await pool.query(sql);
};

const getUsersBy = async (
  searchBy: string,
  column: string,
  limit: number,
  offset: number
) => {
  const sql = `
  SELECT *
  FROM users WHERE ${column} LIKE '%${searchBy}%'
  LIMIT ? OFFSET ?;
  `;
  return await pool.query(sql, [limit, offset]);
};

const getSearchCount = async (searchBy: string, column: string) => {
  const sql = `
  SELECT COUNT(*) as count FROM users 
  WHERE ${column} LIKE '%${searchBy}%' 
  `;

  return await pool.query(sql);
};

export default {
  createUser,
  getUserBy,
  getAllUsers,
  allUsersCount,
  getUsersBy,
  getSearchCount,
};
