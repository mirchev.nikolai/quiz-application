import serviceErrors from './service-errors.js';
import bcrypt from 'bcrypt';

const getAllUsers = (data: {
  getUsersBy: Function;
  getSearchCount: Function;
  getAllUsers: Function;
  allUsersCount: Function;
}) => {
  return async (search: undefined | string, column: string, page: number) => {
    const limit = 20;
    const offset = (page - 1) * limit;

    if (search) {
      const users = await data.getUsersBy(search, column, limit, offset);
      const [{ count }] = await data.getSearchCount(search, column);

      return {
        users: users,
        count: count,
        currentPage: page,
        hasNext: offset + limit < count,
        hasPrevious: page > 1,
      };
    } else {
      const users = await data.getAllUsers(limit, offset);
      const [{ count }] = await data.allUsersCount(search, column);

      return {
        users: users,
        count: count,
        currentPage: page,
        hasNext: offset + limit < count,
        hasPrevious: page > 1,
      };
    }
  };
};

const registerUser = (data: { getUserBy: Function; createUser: Function }) => {
  return async (createData: {
    username: string;
    firstname: string;
    lastname: string;
    password: string;
  }) => {
    const { username, firstname, lastname, password } = createData;

    const existingUser = await data.getUserBy('username', username);

    if (existingUser) {
      return {
        error: serviceErrors.DUPLICATE_RECORD,
        user: null,
      };
    }

    const passwordHash = await bcrypt.hash(password, 10);

    const user = await data.createUser(
      username,
      passwordHash,
      firstname,
      lastname
    );

    return { error: null, user: user };
  };
};

const signInUser = (data: { getUserBy: Function }) => {
  return async (username: string, password: string) => {
    const user = await data.getUserBy('username', username);

    if (!user || !(await bcrypt.compare(password, user.password))) {
      return {
        error: serviceErrors.INVALID_SIGNIN,
        user: null,
      };
    }

    return {
      error: null,
      user: user,
    };
  };
};

export default {
  getAllUsers,
  registerUser,
  signInUser,
};
