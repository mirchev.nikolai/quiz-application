import { Radio, TextField } from '@material-ui/core';
import React, { useState } from 'react';

const SingleChoiceAnswer: React.FC = () => {
    const [selectedValue, setSelectedValue] = useState<string | null>(null);

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSelectedValue(event.target.value);
    };

    return (
        <div>
            <Radio
                checked={selectedValue === "1"}
                onChange={handleChange}
                value="1"
                name="radio-button-demo"
                inputProps={{ 'aria-label': 'C' }}
            />
            <TextField id="standard-basic" label="answer..." size="small"/>
        </div>
    );
}

export default SingleChoiceAnswer;