import { Quiz } from '../customTypes';
import quizFormatter from '../utils/quizFormatter';
import serviceErrors from './service-errors';

const getQuiz = (data: any) => {
    return async (quizId: number) => {
        const rawQuizData = await data.getQuizById(quizId);

        if (!rawQuizData[0]) {
            return {
                error: serviceErrors.RECORD_NOT_FOUND,
                quiz: null
            }
        } else {
            const formattedQuiz = quizFormatter(rawQuizData);
            return {
                error: null,
                quiz: formattedQuiz
            }
        }
    }
}

const checkQuiz = (data: any) => {
    return async (name: string) => {
        const quiz = await data.checkQuizByName(name);

        if (quiz) {
            return {
                error: serviceErrors.DUPLICATE_RECORD,
                quiz: null
            }
        } else {
            return {
                error: null,
                quiz: quiz
            }
        }
    }
}

const postQuiz = (data: any) => {
    return async (quiz: Quiz) => {
        const { title } = quiz;

        // check for existing quiz with the same name
        const exists = await data.checkQuizByName(title);

        if (exists) {
            return {
                error: serviceErrors.DUPLICATE_RECORD,
                quiz: null
            }
        } else {
            const _ = await data.createQuiz(quiz);
            return {
                error: null,
                quiz: `A new quiz with title '${title}' has been successfully created!`
            }
        }
    }
}

export default {
    getQuiz,
    checkQuiz,
    postQuiz
};