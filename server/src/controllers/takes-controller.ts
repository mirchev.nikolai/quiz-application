import express from 'express';
import { Request, Response } from 'express';
import serviceErrors from '../services/service-errors';
import takesData from '../data/takes-data';
import takesService from '../services/takes-service';
import { bodyValidator } from '../validations/validation-exports.js';

const takesController = express.Router();

takesController
  // GET SINGLE STUDENT AVG SCORE ON ALL QUIZZES
  .get('/student', async (req: Request, res: Response) => {
    const user: {
      role?: string;
      id?: string;
    } = req.user!;

    const { error, result } = await takesService.studentScore(takesData)(user);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(409).send({ message: 'No records were found', result: null });
    } else {
      res.status(201).send({ error: null, result: result });
    }
  })

  // GET SINGLE STUDENT SCORE ON A SINGLE QUIZ
  .get('/student/:id', async (req: Request, res: Response) => {
    const user: {
      role?: string;
      id?: string;
    } = req.user!;

    const { id } = req.params;

    const { error, result } = await takesService.scoreOnTake(takesData)(
      +id,
      user
    );

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(409).send({ message: 'No records were found', result: null });
    } else {
      res.status(201).send({ error: null, result: result });
    }
  })

  // ATEMPT A QUIZ
  .get('/:id', async (req: Request, res: Response) => {
    const { id } = req.params;
    const user: {
      role?: string;
      id?: string;
    } = req.user!;

    const { error, take } = await takesService.takeQuiz(takesData)(+id, user);

    if (error === serviceErrors.ALREADY_TAKEN) {
      res.status(409).send({ message: 'You can do this quiz just once' });
    } else if (error === serviceErrors.ANOTHER_ACTIVE_QUIZ) {
      res
        .status(409)
        .send({ message: 'You can have only 1 quiz active at a time' });
    } else {
      res.status(201).send({ error: null, take: take });
    }
  })

  // SUBMIT ANSWERS
  .put('/:id', async (req: Request, res: Response) => {
    const inputBody: {
      take_answers: string[];
    } = req.body;
    const { id } = req.params;
    const user: {
      role?: string;
      id?: string;
    } = req.user!;

    const answers = inputBody.take_answers;

    const { error, submission } = await takesService.submitQuiz(takesData)(
      +id,
      user,
      answers
    );

    if (error === serviceErrors.INVALID_OPERATION) {
      res
        .status(409)
        .send({ message: 'You can not submit this quiz!', submission: null });
    }
    else{
      res.status(201).send({ error: null, submission: submission });
    }
  });

export default takesController;
