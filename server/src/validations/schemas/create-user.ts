export const createUserSchema = {
  username: (value: string) => {
    if (!value) {
      return 'Username is required';
    }

    if (
      typeof value !== 'string' ||
      value.trim().length < 3 ||
      value.trim().length > 25
    ) {
      return 'Username should be a string in range [3..25]';
    }

    return null;
  },
  firstname: (value: string) => {
    if (!value) {
      return 'Your first name is required';
    }

    if (
      typeof value !== 'string' ||
      value.trim().length < 2 ||
      value.trim().length > 25
    ) {
      return 'First name should be a string in range [2..25]';
    }

    return null;
  },
  lastname: (value: string) => {
    if (!value) {
      return 'Your last name is required';
    }

    if (
      typeof value !== 'string' ||
      value.trim().length < 2 ||
      value.trim().length > 25
    ) {
      return 'Last name should be a string in range [2..25]';
    }

    return null;
  },
  password: (value: string) => {
    if (!value) {
      return 'Password is required';
    }

    if (typeof value !== 'string' || value.length < 4 || value.length > 25) {
      return 'Password should be a string in range [4..25]';
    }

    return null;
  },
};
