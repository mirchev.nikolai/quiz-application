import { Question, Quiz, RawQuizData } from "../customTypes";

const quizFormatter = (quiz: RawQuizData[]) => {
  const questionsSet: Set<number> = new Set();
  quiz.forEach((entry: RawQuizData) => {
    questionsSet.add(entry.questionID);
  });
  const questionIndexes = [...questionsSet];
  const formattedQuiz: Quiz = {
    id: quiz[0].quizID,
    userId: quiz[0].quizCreatorID,
    category: quiz[0].quizCategory,
    title: quiz[0].quizTitle,
    questions: [],
  };

  questionIndexes.forEach((qIndex) => {
    let question: Question | undefined;

    quiz.map((el: RawQuizData) => {
      if (!question && el.questionID === qIndex) {
        question = {
          id: el.questionID,
          name: el.questionText,
          points: el.questionPoints,
          answers: [],
        };
      }

      if (el.questionID === qIndex) {
        const answer = {
          id: el.answerID,
          text: el.answerText,
          is_correct: el.is_correct,
        };
        if (question) {
          question.answers.push(answer);
        }
      }
    });
    formattedQuiz.questions.push(question!);
  });
  return formattedQuiz;
};

export default quizFormatter;
