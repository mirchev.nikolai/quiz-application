import React from 'react';
import PropTypes from 'prop-types';

const AppError: React.FC = (message) => {
  return (
    <div className='AppError'>
      <h1>{message}</h1>
    </div>
  );
};

AppError.propTypes = {
  message: PropTypes.string.isRequired,
};

export default AppError;
