import React from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import Dashboard from './Dashboard/Dashboard';
import NavBar from './NavBar/NavBar';
const TeacherApp: React.FC = () => {
  return (
    <BrowserRouter>
      <NavBar />
      <div className='main-content'>
        <Switch>
          <Redirect path='/' exact to='/dashboard' />
          <Redirect path='/login' exact to='/dashboard' />
          <Route path='/dashboard' component={Dashboard} />
          <Route path='*' component={Dashboard} />
        </Switch>
      </div>
    </BrowserRouter>
  );
};

export default TeacherApp;