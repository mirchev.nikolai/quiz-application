import serviceErrors from './service-errors.js';

const getScores = (data: {
  searchScoresByCat: Function;
  searchScoresCount: Function;
  getAllScores: Function;
  getAllScoresCount: Function;
}) => {
  return async (search: undefined | string, column: string, page: number) => {
    const limit = 20;
    const offset = (page - 1) * limit;

    if (search) {
      const result = await data.searchScoresByCat(
        search,
        column,
        limit,
        offset
      );
      const [{ count }] = await data.searchScoresCount(search, column);

      return {
        result: result,
        count: count,
        currentPage: page,
        hasNext: offset + limit < count,
        hasPrevious: page > 1,
      };
    } else {
      const result = await data.getAllScores(limit, offset);
      const [{ count }] = await data.getAllScoresCount();

      return {
        result: result,
        count: count,
        currentPage: page,
        hasNext: offset + limit < count,
        hasPrevious: page > 1,
      };
    }
  };
};

const getTopScores = (data: { topScores: Function }) => {
  return async () => {
    const result = await data.topScores();
    return result;
  };
};

const getQuizScores = (data: {
  findQuiz: Function;
  quizScoresCount: Function;
  quizScores: Function;
}) => {
  return async (id: number, page: number) => {
    const limit = 20;
    const offset = (page - 1) * limit;

    const quiz = await data.findQuiz(id);
    if (!quiz) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        result: null,
      };
    }

    const count = await data.quizScoresCount(id);

    const result = await data.quizScores(id, limit, offset);
    return {
      result: result,
      count: count,
      currentPage: page,
      hasNext: offset + limit < count,
      hasPrevious: page > 1,
    };
  };
};

export default {
  getScores,
  getTopScores,
  getQuizScores,
};
