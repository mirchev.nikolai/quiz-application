const get = (url: string) => {
    const token = localStorage.getItem('token');
  
    return fetch(url, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }).then((res) => res.json());
  };
  
  const post = (url: string, body: any) => {
    const token = localStorage.getItem('token');
  
    return fetch(url, {
      body: JSON.stringify(body),
      method: 'POST',
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
    }).then((res) => res.json());
  };
  
  const put = (url: string, body: any) => {
    const token = localStorage.getItem('token');
    return fetch(url, {
      body: JSON.stringify(body),
      method: 'PUT',
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
    }).then((res) => res.json());
  };
  
  const remove = (url: string) => {
    const token = localStorage.getItem('token');
  
    return fetch(url, {
      method: 'DELETE',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }).then((res) => res.json());
  };
  
  const httpProvider = {
    get,
    post,
    put,
    remove,
  };
  
  export default httpProvider;