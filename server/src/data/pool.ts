import mariadb from 'mariadb';
import { DATABASE_CONFIG } from '../config';

const pool = mariadb.createPool(DATABASE_CONFIG);

export default pool;