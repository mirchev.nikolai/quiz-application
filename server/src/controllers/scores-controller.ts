import express, { Request, Response } from 'express';
import serviceErrors from '../services/service-errors';
import scoresData from '../data/scores-data';
import scoresService from '../services/scores-service';
import e from 'express';

const scoresController = express.Router();

scoresController
  //GET TOP SCORES OPTIONAL SEARCH PER CATEGORY
  .get('/', async (req: Request, res: Response) => {
    let search;
    let column;
    let page;

    if (req.query && req.query.search) {
      search = (req.query as any).search;
    }
    if (req.query && req.query.column) {
      column = (req.query as any).column;
    }
    if (req.query && req.query.page) {
      page = (req.query as any).page;
    }

    const result = await scoresService.getScores(scoresData)(
      search,
      column ? column : 'name',
      page ? +page : 1
    );
    res.status(200).send(result);
  })

  // GET TOP 10 SCORES OVERALL
  .get('/top', async (req: Request, res: Response) => {
    const result = await scoresService.getTopScores(scoresData)();
    res.status(200).send(result);
  })

  // GET SCORES ON A SINGLE QUIZ
  .get('/quiz/:id/', async (req: Request, res: Response) => {
    const { id } = req.params;
    let page;

    if (req.query && req.query.page) {
      page = (req.query as any).page;
    }

    const { error, ...rest } = await scoresService.getQuizScores(scoresData)(
      +id,
      page ? +page : 1
    );
    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(409).send({ message: 'Quiz was not found!' });
    } else {
      res.status(200).send(rest);
    }
  });

export default scoresController;
