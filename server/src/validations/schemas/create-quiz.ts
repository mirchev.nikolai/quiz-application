import { Answer, Question } from './../../customTypes';

export const createQuizSchema = {
  category: (value: number) => {
    if (typeof value !== "number" || value < 0) {
      return "Category ID must be a positive integer!";
    }
    return null;
  },
  userId: (value: number) => {
    if (typeof value !== "number" || value < 0) {
      return "User ID must be a positive integer!";
    }
    return null;
  },
  title: (value: string) => {
    if (
      typeof value !== "string" ||
      value.trim().length < 3 ||
      value.trim().length > 256
    ) {
      return "Title should be a string in range [2..256]";
    }
    return null;
  },
  questions: (value: Question[]) => {
    if (value.length < 2) {
      return "A quiz must have at least 2 questions!";
    }
    const validator = { error: false, message: "" };
    value.every((el: Question) => {
      if (
        typeof el.name !== "string" ||
        el.name.trim().length < 3 ||
        el.name.trim().length > 256
      ) {
        validator.error = true;
        validator.message =
          "A question's name must be a string in range [3..256]";
        return false;
      }
      if (typeof el.points !== "number" || el.points < 1 || el.points > 6) {
        validator.error = true;
        validator.message = "Points must be a number in range [1..6]";
        return false;
      }
      if (el.answers.length < 2) {
        validator.error = true;
        validator.message = "Each question must have at least 2 answers!";
        return false;
      }
      let countCorrectAnswers: number = 0;
      el.answers.every((answer: Answer) => {
        if (
          typeof answer.text !== "string" ||
          answer.text.trim().length < 1 ||
          answer.text.trim().length > 256
        ) {
          validator.error = true;
          validator.message =
            "An answer's text must be a string in range [1..256]";
          return false;
        }
        if (
          typeof answer.is_correct !== "number" ||
          answer.is_correct < 0 ||
          answer.is_correct > 1
        ) {
          validator.error = true;
          validator.message =
            "The is_correct value should be number - either 0 or 1";
          return false;
        }
        if (answer.is_correct) {
          countCorrectAnswers++;
        }
      });
      if (!countCorrectAnswers) {
        validator.error = true;
        validator.message = `Question titled '${el.name}' has no correct answers - there must be at least one!`;
        return false;
      }
    });
    if (validator.error) {
      return validator.message;
    } else {
      return null;
    }
  },
};
