import express, { Request, Response } from 'express';
import serviceErrors from '../services/service-errors';
import categoriesData from '../data/categories-data';
import categoriesService from '../services/categories-service';
import {
  bodyValidator,
  createCategorySchema,
} from '../validations/validation-exports.js';
import { roleMiddleware } from '../auth/auth-middleware';
import { teachersOnly } from '../config';

const categoryController = express.Router();

categoryController
  //GET ALL CATEGORIES WITH OPTIONAL SEARCH
  .get('/', async (req: Request, res: Response) => {
    let search;
    let column;
    let page;

    if (req.query && req.query.search) {
      search = (req.query as any).search;
    }
    if (req.query && req.query.column) {
      column = (req.query as any).column;
    }
    if (req.query && req.query.page) {
      page = (req.query as any).page;
    }

    const result = await categoriesService.getAllCategoriesPaged(
      categoriesData
    )(search, column ? column : 'name', page ? +page : 1);

    res.status(200).send(result);
  })

  // GET ALL CATEGORIES WITHOUT PAGINATION
  .get('/all', async (req: Request, res: Response) => {
    const result = await categoriesService.getAllCategories(categoriesData)();

    res.status(200).send(result);
  })

  // GET CATEGORY BY ID
  .get('/:id', async (req: Request, res: Response) => {
    const { id } = req.params;

    const { error, category } = await categoriesService.getCategoryById(
      categoriesData
    )(id);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).send({ message: 'Category was not found!' });
    } else {
      res.status(200).send(category);
    }
  })

  // CREATE A CATEGORY
  .put(
    '/',
    roleMiddleware(teachersOnly),
    bodyValidator(createCategorySchema),
    async (req: Request, res: Response) => {
      const categoryData:{
        name: string;
        description: string;
        images_id: string;
      } = req.body;
      const user: {
        id?: string;
      } = req.user!;
      const userId = user.id!;

      const { error, category } = await categoriesService.createCategory(
        categoriesData
      )(categoryData, +userId);

      if (error === serviceErrors.DUPLICATE_RECORD) {
        res
          .status(409)
          .send({ message: 'Category already exists!', category: null });
      } else {
        res.status(201).send(category);
      }
    }
  );

export default categoryController;
