import { Button } from '@material-ui/core';
import React, { useState } from 'react';
import MultiChoiceAnswer from './MultiChoiceAnswer';

const MultiAnswerQuestion: React.FC = () => {
    const [answers, setAnswers] = useState([
        <div key={1}><MultiChoiceAnswer /></div>,
        <div key={2}><MultiChoiceAnswer /></div>,
    ]);

    const removeLastAnswer = () => {
        if (answers.length > 2) {
            setAnswers(answers.slice(0, -1))
        }
    }

    return(
        <div className="answers">
        {answers && answers.map((answer, index) => <div key={index + 2}>{answer}</div>)}
        <Button size="small" variant="contained" color="primary" onClick={() => setAnswers([...answers, <MultiChoiceAnswer />])}>Add Answer</Button>
        &nbsp;
        <Button size="small" variant="outlined" color="primary" onClick={removeLastAnswer}>Remove Last</Button>
        </div>
    );
}

export default MultiAnswerQuestion;