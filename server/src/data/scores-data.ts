import pool from './pool.js';

const getAllScores = async (limit: number, offset: number) => {
  const sql: string = `
  SELECT users.firstname, users.lastname, users.username, SUM(takes.result) AS result 
  FROM takes,users 
  WHERE users.id=takes.users_id 
  AND users.roles_id = 2
  AND takes.is_submitted=1
  GROUP BY users_id
  ORDER BY result 
  DESC
  LIMIT ? OFFSET ?;
  `;

  const result = await pool.query(sql, [limit, offset]);
  return result;
};

const getAllScoresCount = async () => {
  const sql: string = `
  SELECT COUNT(DISTINCT takes.users_id) as count
	FROM takes,users 
  WHERE users.id=takes.users_id 
	AND users.roles_id = 2
	AND takes.result <> -1
	AND takes.is_submitted = 1;
  `;

  const result = await pool.query(sql);
  return result;
};

const searchScoresByCat = async (
  search: string,
  column: string,
  limit: number,
  offset: number
) => {
  const sql: string = `
  SELECT users.firstname, users.lastname, users.username, SUM(takes.result) AS result, quiz.id as quiz_id,quiz.title as quiz_name, categories.id as categories_id , categories.name as category_name
  FROM takes,users,quiz,categories
  WHERE LOWER(categories.${column}) = ?
  AND categories.id=quiz.category_id 
  AND takes.quiz_id = quiz.id
  AND users.id=takes.users_id 
  AND users.roles_id = 2
  AND takes.is_submitted=1
  GROUP BY takes.users_id
  ORDER BY result 
  DESC
  LIMIT ? OFFSET ?;
  `;

  const result = await pool.query(sql, [search, limit, offset]);
  return result;
};

const searchScoresCount = async (search: string, column: string) => {
  const sql: string = `
  SELECT COUNT(takes.users_id) as count
  FROM takes,users,quiz,categories
  WHERE LOWER(categories.${column}) = '${search}'
  AND categories.id=quiz.category_id 
  AND takes.quiz_id = quiz.id
  AND users.id=takes.users_id 
  AND users.roles_id = 2
  AND takes.is_submitted=1;
  `;

  const result = await pool.query(sql);
  return result;
};

const topScores = async () => {
  const sql: string = `
  SELECT users.firstname, users.lastname, users.username, SUM(takes.result) AS result 
  FROM takes,users 
  WHERE users.id=takes.users_id 
  AND users.roles_id = 2
  AND takes.is_submitted=1
  GROUP BY users_id
  ORDER BY result
  DESC
  LIMIT 10;  
  `;

  const result = await pool.query(sql);
  return result;
};

const findQuiz = async (id: number) => {
  const sql: string = `
  SELECT * 
  FROM quiz 
  WHERE id = ?
  `;

  const result = await pool.query(sql, [id]);
  return result[0];
};

const quizScores = async (id: number, limit: number, offset: number) => {
  const sql: string = `
  SELECT * 
  FROM takes 
  WHERE quiz_id = ? 
  AND result <> -1 
  ORDER BY result 
  DESC
  LIMIT ? OFFSET ?;
  `;

  const result = await pool.query(sql, [id, limit, offset]);
  return result;
};

const quizScoresCount = async (id: number) => {
  const sql: string = `
  SELECT COUNT(id) as count
  FROM takes 
  WHERE quiz_id = ? 
  AND result <> -1;
  `;

  const result = await pool.query(sql, [id]);
  return result[0].count;
};

export default {
  getAllScores,
  getAllScoresCount,
  searchScoresByCat,
  searchScoresCount,
  topScores,
  findQuiz,
  quizScores,
  quizScoresCount,
};
