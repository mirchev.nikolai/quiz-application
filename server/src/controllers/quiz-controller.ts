import express, { Request, Response } from 'express';
import serviceErrors from '../services/service-errors';
import quizData from '../data/quiz-data';
import quizService from '../services/quiz-service';
import { bodyValidator, createQuizSchema } from '../validations/validation-exports';
import { Quiz } from '../customTypes';

const quizController = express.Router();

quizController
// Get quiz by id
.get('/:id', async (req: Request, res: Response) => {
    const { id } = req.params;

    const { error, quiz } = await quizService.getQuiz(quizData)(+id);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
        return res.status(404).send({
            message: `Quiz with an id ${id} does not exist!`
        });
    }

    res.status(200).send(quiz);
})

// Post (create) new quiz
.post('/create', bodyValidator(createQuizSchema), async (req: Request, res: Response) => {
    const data: Quiz = req.body;
    const { title } = data;

    const { error, quiz } = await quizService.postQuiz(quizData)(data);

    if (error === serviceErrors.DUPLICATE_RECORD) {
        return res.status(409).send({
            message: `Quiz with name '${title}' already exists!`
        });
    } else {
        res.status(200).send({ message: quiz});
    }
})

export default quizController;