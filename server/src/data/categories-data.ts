import pool from './pool.js';

const getCategoryBy = async (
  searchBy: string,
  column: string,
  limit: number,
  offset: number
) => {
  const sql = `
  SELECT c.id, c.users_id, c.name, c.description, c.images_id, i.path
  FROM categories as c, images as i
  WHERE LOWER(c.${column}) LIKE '%${searchBy}%' AND i.id = c.images_id
  LIMIT ? OFFSET ?;
  `;

  return await pool.query(sql, [limit, offset]);
};

const getSearchCategoryCount = async (searchBy: string, column: string) => {
  const sql = `
  SELECT COUNT(*) as count FROM categories 
  WHERE ${column} LIKE '%${searchBy}%' 
  `;

  return await pool.query(sql);
};

const getAllCategories = async (limit: number, offset: number) => {
  const sql = `
  SELECT c.id, c.users_id, c.name, c.description, c.images_id, i.path
  FROM categories as c, images as i
  WHERE i.id = c.images_id
  LIMIT ? OFFSET ?;
  `;
  return await pool.query(sql, [limit, offset]);
};

const allCategoriesCount = async () => {
  const sql = `
  SELECT COUNT(*) as count FROM categories 
  `;

  return await pool.query(sql);
};

const categoryBy = async (column: string, getBy: string | number) => {
  const sql = `
  SELECT c.id, c.users_id, c.name, c.description, c.images_id, i.path
  FROM categories as c, images as i
  WHERE c.${column} = ? && i.id = c.images_id;
  `;
  const result = await pool.query(sql, [getBy]);
  return result[0];
};

const allCategories = async () => {
  const sql = `
  SELECT c.id, c.users_id, c.name, c.description, c.images_id, i.path
  FROM categories as c, images as i
  WHERE i.id = c.images_id;
  `;

  return await pool.query(sql);
};

const createCategory = async (
  name: string,
  description: string,
  image: number,
  userId: number
) => {
  const sql = `
  INSERT INTO categories(name, description, images_id, users_id)
  VALUES (?, ?, ?, ?)
  `;

  const result = await pool.query(sql, [name, description, image, userId]);

  return {
    id:result.insertId,
    name: name,
    description: description,
    image: image,
    userId: userId,
  };
};

export default {
  getCategoryBy,
  getSearchCategoryCount,
  getAllCategories,
  allCategoriesCount,
  categoryBy,
  allCategories,
  createCategory,
};
