export const createCategorySchema = {
  name: (value: string) => {
    if (!value) {
      return 'Name of the category is required';
    }
    if (
      typeof value !== 'string' ||
      value.trim().length < 2 ||
      value.trim().length > 25
    ) {
      return 'The title should be a string in range [2..25]';
    }
    return null;
  },
  description: (value: string) => {
    if (!value) {
      return 'Description of the category is required';
    }
    if (
      typeof value !== 'string' ||
      value.trim().length < 3 ||
      value.trim().length > 1250
    ) {
      return 'The title should be a string in range [3..1250]';
    }
    return null;
  },
  images_id: (value: string) => {
    if (!value) {
      return 'Please select image for this quiz';
    }
    return null;
  },
};
