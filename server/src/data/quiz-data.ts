import { Quiz } from '../customTypes.js';
import pool from './pool.js';

const getQuizById = async (id: number) => {
    const sql: string = `
    SELECT q.id as quizID, q.title as quizTitle, q.category_id as quizCategory, q.users_id as quizCreatorID,
            qs.id as questionID, qs.text as questionText, qs.points as questionPoints,
            a.id as answerID, a.text as answerText, a.is_correct
    FROM quiz as q, questions as qs, answers as a
    WHERE q.id = ? AND qs.quiz_id = ? AND qs.id = a.questions_id;
    `;

    return await pool.query(sql, [id, id]);
};

const checkQuizByName = async (name: string) => {
    const sql: string = `
        SELECT q.title
        FROM quiz AS q
        WHERE q.title = ?;
    `;
    const result = await pool.query(sql, [name]);
    return result[0];
};

const createQuiz = async (quiz: Quiz) => {
    let sql: string = `INSERT INTO quiz(title, category_id, users_id)
        VALUES ("${quiz.title}", ${quiz.category}, ${quiz.userId});
        SELECT id INTO @QUIZ_ID FROM quiz WHERE quiz.title = "${quiz.title}";
    `;

    const questions = [...quiz.questions];

    for (const question of questions) {
        sql += `
            INSERT INTO questions(quiz_id, text, points)
            VALUES (@QUIZ_ID, "${question.name}", ${question.points});
            SELECT id INTO @QUESTION_ID FROM questions AS q WHERE q.text = "${question.name}" AND q.quiz_id = @QUIZ_ID;
        `;

        const answers = [...question.answers];

        for (const answer of answers) {
            sql += `
                INSERT INTO answers(questions_id, text, is_correct)
                VALUES (@QUESTION_ID, "${answer.text}", ${answer.is_correct});
        `;
        };
    };
    return await pool.query(sql);
}

export default {
    getQuizById,
    checkQuizByName,
    createQuiz
};